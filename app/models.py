from root.models import Model
from django.db import models
from django.contrib.auth.models import AbstractBaseUser


class User(Model, AbstractBaseUser):
    username = models.CharField(max_length=50, unique=True)
    phone = models.CharField(max_length=25, null=True, blank=True)

    REQUIRED_FIELDS = ['username']
